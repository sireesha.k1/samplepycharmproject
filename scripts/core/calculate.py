import math


class Shape:
    @staticmethod
    def rectangle(l, b):
        area = l * b
        perimeter = (2 * (l + b))
        return {"Area": area, "Perimeter": perimeter}

    @staticmethod
    def square(l):
        area = l * l
        perimeter = 4 * l
        return {"Area": area, "Perimeter": perimeter}

    @staticmethod
    def triangle(a, b, c, h):
        area = 0.5 * b * h
        perimeter = a + b + c
        return {"Area": area, "Perimeter": perimeter}

    @staticmethod
    def circle(r):
        area = math.pi * r * r
        perimeter = 2 * math.pi * r
        return {"Area": area, "Perimeter": perimeter}
