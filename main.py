from scripts.core.calculate import Shape

_object = Shape()

if __name__ == "__main__":
    print("Calculating area and perimeter for shape")
shape = input("Enter shape to calculate area and perimeter:")
shape = shape.lower()
if (shape == "rectangle"):
    l, b = map(int, input("Enter length and breadth:").split())
    print(_object.rectangle(l, b))
elif (shape == "square"):
    s = int(input("Enter side:"))
    print(_object.square(s))
elif (shape == "triangle"):
    a, b, c, h = map(int, input("Enter 3 sides and height:").split())
    print(_object.triangle(a, b, c, h))
elif (shape == "circle"):
    r = int(input("Enter radius:"))
    print(_object.circle(r))
else:
    print("There is no such shape.")

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
